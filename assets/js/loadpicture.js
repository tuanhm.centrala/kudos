var folderPath = "./images/";
var pictureArray = [];

function getPicturesFromFolder(folderPath) {
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4 && xhr.status === 200) {
      var parser = new DOMParser();
      var htmlDoc = parser.parseFromString(xhr.responseText, "text/html");
      var links = htmlDoc.getElementsByTagName("a");

      for (var i = 0; i < links.length; i++) {
        var filename = links[i].getAttribute("href");

        if (/\.(jpe?g|png|gif)$/i.test(filename)) {
          pictureArray.push(filename);
        }
      }

      printPictureArray();
    }
  };
  xhr.open("GET", folderPath, true);
  xhr.send();
}

function printPictureArray() {
  for (var i = 0; i < pictureArray.length; i++) {
    console.log(pictureArray[i]);
  }
}

getPicturesFromFolder(folderPath);
