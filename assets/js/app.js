$(document).ready(function(){

  $(".nav-gofast li").click(function(){
    $(this).children('.sub-menu').slideToggle("slow");
  });

    // menu mobi:
    $('.menu-toggle').click(function(){
      $(".all-nav-gofast").toggleClass("mobile-nav");
      $(this).toggleClass("is-active");
      $("body").toggleClass("block-body");
      });
   //----------------------
      const counters = document.querySelectorAll(".counter");
      counters.forEach((counter) => {
        counter.innerText = "0";
        const updateCounter = () => {
          const target = +counter.getAttribute("data-target");
          const count = +counter.innerText;
          const increment = target / 200;
          if (count < target) {
            counter.innerText = `${Math.ceil(count + increment)}`;
            setTimeout(updateCounter, 1);
          } else counter.innerText = target;
        };
        updateCounter();
      });
      


});
