// #=include ../js/animation.js
/*
     _ _      _       _
 ___| (_) ___| | __  (_)___
/ __| | |/ __| |/ /  | / __|
\__ \ | | (__|   < _ | \__ \
|___/_|_|\___|_|\_(_)/ |___/
                   |__/

 Version: 1.8.0
  Author: Ken Wheeler
 Website: http://kenwheeler.github.io
    Docs: http://kenwheeler.github.io/slick
    Repo: http://github.com/kenwheeler/slick
  Issues: http://github.com/kenwheeler/slick/issues

 */
/* global window, document, define, jQuery, setInterval, clearInterval */
;(function(factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (typeof exports !== 'undefined') {
        module.exports = factory(require('jquery'));
    } else {
        factory(jQuery);
    }

})




window.Animation = function (options) {
  //  SCOPE
  /// ---------------------------      
  const that = this;
  const viewportWidth = window.innerWidth;
  const tabletView = $(".tablet.landscape");
  let activeLocomotive = true;

  //  OPTIONS
  /// ---------------------------      
  options = options || {};
  options.$el = options.$el;
  options.listTextReveal = options.hasOwnProperty('listTextReveal') ? options.listTextReveal : false;
  options.$scaleImg = options.hasOwnProperty('$scaleImg') ? options.$scaleImg : false;
  options.$zoomOut = options.hasOwnProperty('$zoomOut') ? options.$zoomOut : false;
  options.splitTextReveal = options.hasOwnProperty('splitTextReveal') ? options.splitTextReveal : false;
  options.scrollHorizontal = options.hasOwnProperty('scrollHorizontal') ? options.scrollHorizontal : false;
  options.imgSequence = options.hasOwnProperty('imgSequence') ? options.imgSequence : false;
  options.hasScrollUpdate = options.hasOwnProperty('hasScrollUpdate') ? options.hasScrollUpdate : false;
  options.cardFading = options.hasOwnProperty('cardFading') ? options.cardFading : false;
  options.imgExpanding = options.hasOwnProperty('imgExpanding') ? options.imgExpanding : false;
  gsap.registerPlugin(ScrollTrigger);

  this.setScroll = function() {
    const scrollOptions = {
      el: options.$el,
      smooth: true,
      getDirection: true,
      // smartphone: { smooth: true },
      // tablet: { smooth: true },
      lerp: 0.05,
      multiplier: 1.2,
      offset: [0, 0],
      reloadOnContextChange: true,
    };

    let self = this;

    self.locoScroll = new LocomotiveScroll(scrollOptions);

    function getInstance(instance) {
      document.documentElement.setAttribute('data-direction', instance.direction)
    }

    self.locoScroll.on("scroll", ScrollTrigger.update);
    self.locoScroll.on("scroll", (instance) => getInstance(instance));

    ScrollTrigger.scrollerProxy(options.$el, {
      scrollTop(value) {
        return arguments.length ? self.locoScroll.scrollTo(value, 0, 0) : self.locoScroll.scroll.instance.scroll.y;
      },
      getBoundingClientRect() {
        return {
          top: 0,
          left: 0,
          width: window.innerWidth,
          height: window.innerHeight
        };
      },
      pinType: options.$el.style.transform ? "transform" : "fixed"
    });

    ScrollTrigger.addEventListener("refresh", () => self.locoScroll.update());
    ScrollTrigger.refresh();

    if (options.hasScrollUpdate) {
      options.hasScrollUpdate.on("click", function () {
        setTimeout( function() {
          self.locoScroll.update();
        }, 500);
      });
    }

    document.addEventListener("click", toggleDropdown);

    function toggleDropdown(event) {
      activeLocomotive = !activeLocomotive;

      if (!event.target.matches('.selectBtn')) {
        self.locoScroll.start();
      } else {
        activeLocomotive === false ? self.locoScroll.stop() : self.locoScroll.start();
      }
    }
  }

  this.onListText = function () {
    const list = gsap.utils.toArray(options.listTextReveal);

    list.forEach(function ($el, i) {
      const revealList = gsap.timeline({
        scrollTrigger: {
          trigger: $el,
          scroller: options.$el,
        }
      });

      revealList.set($el, { autoAlpha: 1 })
        .from($el, {
          yPercent: 100,
          animation: !0,
          duration: 1.2,
          stagger: 0.2,
        });
    });
  };

  this.onScaleImg = function () {
    const scaleImg = gsap.utils.toArray(options.$scaleImg);

    scaleImg.forEach(function ($el, i) {
      const imgZoom = $el.querySelector('.innerInOut');
      const zoom = gsap.timeline({
        scrollTrigger: {
          trigger: imgZoom,
          scroller: options.$el,
          scrub: 0.7,
          start: "top bottom",
          end: "bottom -100%",
          ease: Elastic.easeOut.config(3, 0.3),
        }
      });

      zoom.fromTo(imgZoom,
        {
          scaleX: 1.14,
          scaleY: 1.14,
        },
        {
          scaleX: 1,
          scaleY: 1,
        }
      );
    });
  }

  this.onZoomOutImg = function () {
    if(options.$zoomOut === false) {
      return;
    }

    const scaleOut = gsap.utils.toArray(options.$zoomOut);

    TweenMax.fromTo(scaleOut, 2, { scale: 1 }, { scale: 1.2, ease: ExpoScaleEase.config(1, 1.2) });
  }

  this.onSplitText = function () {
    if (options.splitTextReveal === false) {
      return;
    }

    const $scroll = $('.smooth-scroll').imagesLoaded( function() {
      function splitWords(selector) {
        const elements = document.querySelectorAll(selector);
        elements.forEach((el) => {
          el.dataset.splitText = el.textContent;
          el.innerHTML = el.textContent
            .split(/\s/)
            .map(function (word) {
              return word
                .split("-")
                .map(function (word) {
                  return '<span class="word">' + word + "</span>";
                })
                .join('<span class="hyphen">-</span>');
            })
            .join('<span class="whitespace"> </span>');
        });
      };
    
      function getLines(el) {
        let lines = [];
        let line;
        const words = el.querySelectorAll("span");
        let lastTop;
        for (let i = 0; i < words.length; i++) {
          let word = words[i];
          if (word.offsetTop != lastTop) {
            if (!word.classList.contains("whitespace")) {
              lastTop = word.offsetTop;
              line = [];
              lines.push(line);
            }
          }
          line.push(word);
        }
        return lines;
      };
    
      function splitLines(selector) {
        splitWords(options.splitTextReveal);
        const elements = document.querySelectorAll(selector);
        elements.forEach( (el) => {
          const lines = getLines(el);
    
          let wrappedLines = "";
          lines.forEach((wordsArr) => {
            wrappedLines += '<span class="line"><span class="words">';
            wordsArr.forEach((word) => {
              wrappedLines += word.outerHTML;
            });
            wrappedLines += "</span></span>";
          });
          el.innerHTML = wrappedLines;
        });
      };

      splitLines(options.splitTextReveal);

      const splitText = gsap.utils.toArray(options.splitTextReveal);

      splitText.forEach(function ($el, i) {
        const lines = $el.querySelectorAll(".words");
        const split = gsap.timeline({
          scrollTrigger: {
            paused: true,
            trigger: $el,
            scroller: options.$el,
          }
        });

        split
          .set($el, {overflow: 'hidden', autoAlpha: 1 })
          .from(lines, 1, {
            yPercent: 100,
            ease: Power3.out,
            duration: 1.4,
            stagger: 0.18,
          });
      });
    });
  }

  this.horizontalScrolling = function () {
    if (options.scrollHorizontal === false) {
      return;
    }

    if (viewportWidth > 1024 && !tabletView.length) {
      const horizontalSections = gsap.utils.toArray(options.scrollHorizontal);
      horizontalSections.forEach(function ($sec, i) {
        const thisPinWrap = $sec.querySelector('.pin-wrap');
        const thisAnimWrap = $sec.querySelector('.animation-wrap');
        gsap.to(thisAnimWrap,
          {
            scrollTrigger: {
              trigger: $sec,
              scroller: options.$el,
              pinType: 'transform',
              start: "top top",
              end: () => "+=" + thisAnimWrap.scrollWidth / 3,
              pin: thisPinWrap,
              scrub: 1,
              anticipatePin: 1,
              invalidateOnRefresh: true,
            },
            x: () => -(thisAnimWrap.scrollWidth - window.innerWidth),
            ease: "none",
          });
      });
    }
  }

	this.imageExpanding = function () {
    if (options.imgExpanding === false) {
      return;
    }

    if (viewportWidth > 1024 && !tabletView.length) {
      const imgExpandingSections = gsap.utils.toArray(options.imgExpanding);
      imgExpandingSections.forEach(function ($sec, i) {
        const thisPinWrap = $sec.querySelector('.scale-pin-wrap');
        const thisAnimWrap = $sec.querySelector('.scaling-img__wrapper');
				const overlayLeft = $sec.querySelector('.scale-img__overlay--left');
				const overlayRight = $sec.querySelector('.scale-img__overlay--right');
				const overlayTop = $sec.querySelector('.scale-img__overlay--top');
				const overlayBottom = $sec.querySelector('.scale-img__overlay--bot');
				const content = $sec.querySelector('.content__scale');
        gsap.to(thisAnimWrap, {
          scrollTrigger: {
            trigger: $sec,
            scroller: options.$el,
            pin: thisPinWrap,
            pinSpacing: true,
            start: "top top",
            end: () => "+=" + thisAnimWrap.scrollWidth,
            scrub: 1,
          }
        });
        gsap.to(overlayLeft,
          {
            scrollTrigger: {
              trigger: $sec,
              scroller: options.$el,
              start: "top top",
              end: "bottom bottom",
              duration: 3,
              scrub: true,
            },
            xPercent: -100,
            stagger: 4,
            ease: "none",
          }
        );

        gsap.to(overlayRight,
          {
            scrollTrigger: {
              trigger: $sec,
              scroller: options.$el,
              start: "top top",
              end: "bottom bottom",
              duration: 3,
              scrub: true,
            },
            xPercent: 100,
            stagger: 4,
            ease: "none",
          }
        );

        gsap.to(overlayTop,
          {
            scrollTrigger: {
              trigger: $sec,
              scroller: options.$el,
              start: "top top",
              end: "bottom bottom",
              duration: 3,
              scrub: true,
            },
            yPercent: -100,
            stagger: 4,
            ease: "none",
          }
        );

        gsap.to(overlayBottom,
          {
            scrollTrigger: {
              trigger: $sec,
              scroller: options.$el,
              start: "top top",
              end: "bottom bottom",
              duration: 3,
              scrub: true,
            },
            yPercent: 100,
            stagger: 4,
            ease: "none",
          }
        );

        gsap.to(content,
          {
            scrollTrigger: {
              trigger: $sec,
              scroller: options.$el,
              start: "top top",
              end: "bottom bottom",
              duration: 3,
              scrub: true,
            },
            opacity: 1,
            ease: "none",
          }
        );

        //gsap.to(overlayLeft, {xPercent: 30})
        // timeline.to(overlayRight, {xPercent: -30})
        // timeline.to(overlayTop, {yPercent: -30})
        // timeline.to(overlayBottom, {yPercent: -30})
      });
    }
  }

  this.onImgSequence = function () {
    if (options.imgSequence === false) {
      return;
    }

    const triggerSequence = document.querySelector(".trigger-sequence");

    gsap.to("#sequence-1, #sequence-2, #sequence-3, #sequence-4",
      {
        scrollTrigger: {
          trigger: triggerSequence,
          scroller: options.$el,
          start: "top top",
          end: "bottom bottom",
          duration: 3,
          scrub: true,
        },
        opacity: 1,
        stagger: 4,
        ease: "none",
      }
    );
  }

  this.cartFading = function () {
    if (options.cardFading === false) {
      return;
    }

    const $gallery = gsap.utils.toArray(options.cardFading);

    $gallery.forEach(function ($gal, i) {
      gsap.fromTo(
        $gal.children,
        { y: '+=30', opacity: 0 },
        {
          y: 0,
          opacity: 1,
          stagger: 0.2,
          duration: 0.7,
          ease: 'easeInOut',
          delay: 0.5,
          scrollTrigger: {
            trigger: $gal,
            scroller: options.$el,
            start: 'top 90%',
          }
        }
      );
    });
  }

  this.init = function () {
    that.setScroll();
    that.onListText();
    that.onScaleImg();
    that.onZoomOutImg();
    that.onSplitText();
    that.horizontalScrolling();
    that.onImgSequence();
    that.cartFading();
		that.imageExpanding();
  }

  this.init();
}

const initAnimation = new Animation({
	$el: document.querySelector(".smooth-scroll"),
	listTextReveal: "[data-text-reveal]",
	$scaleImg: document.querySelectorAll('.imgZoom'),
	imgExpanding: document.querySelectorAll('.scaling-img'),
});

$('.feature-catalog .feature-slider').slick({
	autoplay: false,
	slidesToShow: 1,
	slidesToScroll: 1,
	arrows: true,
	dots: true,
	infinite: false,
	prevArrow: '<button class="slick-prev" aria-label="Previous" type="button">Previous</button>',
	nextArrow: '<button class="slick-next" aria-label="Next" type="button">Next</button>',
	responsive: [
		{
		  breakpoint: 1023,
		  settings: {
			arrows: false,
		  }
		},
	]
});

$('.manuals .manuals-slider').slick({
	autoplay: false,
	slidesToShow: 4.4,
	slidesToScroll: 1,
	arrows: true,
	dots: false,
	infinite: false,
	centerPadding: '20px',
	prevArrow: '<button class="slick-prev" aria-label="Previous" type="button">Previous</button>',
	nextArrow: '<button class="slick-next" aria-label="Next" type="button">Next</button>',
	responsive: [
		{
		  breakpoint: 1023,
		  settings: {
			slidesToShow: 1.4,
			arrows: false,
		  }
		},
	]
});