document.addEventListener("DOMContentLoaded", () => {
    const panels = document.querySelectorAll('.gallery__panel');
    const panelsHeight = panels.length * window.outerHeight;
    const next = 1;
    const fade = 0.1;
    const st = 0.3;

    const tlt = gsap.timeline({
      defaults:{ease:'none', stagger:next},
      scrollTrigger: {
        trigger: ".gallery",
        duration: 1.5,
        pin: true,
        scrub: 0.3,
        start: "top top",
        end: `+=${panelsHeight}`,
      }
    });

    gsap.set(".gallery__panel", {zIndex: (i, target, targets) => targets.length - i});
    const allBgs = gsap.utils.toArray(".thumb");
    allBgsNotLast = allBgs.slice(0, -1);
    const allContents = gsap.utils.toArray(".content"); 
    allContentsNotLast = allContents.slice(0, -1);

    tlt.to(allBgsNotLast, {autoAlpha:0, duration:fade}, next)
    tlt.to(allBgs, {scale:1.1, transformOrigin:'center', duration:next/2}, next/2)
    tlt.to({}, {duration:0.5})
    tlt.to(allContents, {autoAlpha:0, yPercent:'=57', duration:st, ease: "easein"}, next)
  });

  document.addEventListener("DOMContentLoaded", () => {
    const panels = document.querySelectorAll('.gallery__panel');
    const panelsHeight = panels.length * window.outerHeight;
    const next = 1;
    const fade = 0.1;
    const st = 0.3;

    const tlt = gsap.timeline({
      defaults:{ease:'none', stagger:next},
      scrollTrigger: {
        trigger: ".gallery",
        duration: 1.5,
        pin: true,
        scrub: 0.3,
        start: "top top",
        end: `+=${panelsHeight}`,
      }
    });

    gsap.set(".gallery__panel", {zIndex: (i, target, targets) => targets.length - i});
    const allBgs = gsap.utils.toArray(".thumb");
    allBgsNotLast = allBgs.slice(0, -1);
    const allContents = gsap.utils.toArray(".content"); 
    allContentsNotLast = allContents.slice(0, -1);

    tlt.to(allBgsNotLast, {autoAlpha:0, duration:fade}, next)
    tlt.to(allBgs, {scale:1.1, transformOrigin:'center', duration:next/2}, next/2)
    tlt.to({}, {duration:0.5})
    tlt.to(allContents, {autoAlpha:0, yPercent:'=57', duration:st, ease: "easein"}, next)
  });
